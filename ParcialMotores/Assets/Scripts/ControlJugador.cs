using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public TextMeshProUGUI textoCantidadRecolectados;
    public TextMeshProUGUI textoGanaste;
    public TextMeshProUGUI textoTiempo; // Nuevo objeto de texto para mostrar el tiempo
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto = 6f;
    public SphereCollider col;
    private bool enElPiso;
    private int saltosRealizados;
    public int maxSaltos = 2;
    private float tiempoRestante = 60f; // Tiempo inicial en segundos
    private int llavesTotales = 5;
    private int llavesRecolectadas = 0;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        cont = 0;

        // Verifica si el componente TextMeshProUGUI est� presente
        if (textoTiempo != null)
        {
            // Configura el texto inicial del tiempo
            textoTiempo.text = "Tiempo: " + Mathf.Ceil(tiempoRestante).ToString();

            // Inicia la corutina para contar el tiempo
            StartCoroutine(ContadorTiempo());
        }
        else
        {
            Debug.LogWarning("El componente TextMeshProUGUI no est� asignado en el Inspector.");
        }

        setearTextos();

        enElPiso = true;
        saltosRealizados = 0;
    }

    private void Update()
    {
        if (transform.position.y < -10f)
        {
            ReiniciarJuego();
        }

        // Permitir saltar solo si est� en el piso o ha tocado el piso anteriormente y no ha alcanzado el l�mite de saltos
        if (Input.GetKeyDown(KeyCode.Space) && (enElPiso || saltosRealizados < maxSaltos))
        {
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z); // Establecer la componente vertical de la velocidad a cero

            // Aplicar una fuerza diferente dependiendo de si es el primer o segundo salto
            float magnitudSaltoActual = (enElPiso) ? magnitudSalto : magnitudSalto * 0.8f; // Salto reducido si est� en el aire

            rb.AddForce(Vector3.up * magnitudSaltoActual, ForceMode.Impulse);
            saltosRealizados++;

            // Desactivar la posibilidad de saltar hasta que toque el suelo
            enElPiso = false;
        }
    }

    public void RecolectarItem(int aumentoRapidez, float aumentoTama�o)
    {
        rapidez += aumentoRapidez;
        transform.localScale *= aumentoTama�o;

        // Reiniciar el estado de salto al recolectar un �tem y restablecer la cantidad de saltos
        enElPiso = true;
        saltosRealizados = 0;
    }
    public void RestarLlave()
    {
        llavesRecolectadas--;
    }

    public int LlavesRestantes()
    {
        return llavesTotales - llavesRecolectadas;
    }
    private bool EstaEnPiso()
    {
        bool enPiso = Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
          col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);

        if (enPiso)
        {
            Debug.Log("En el piso");
        }

        return enPiso;
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Monedas: " + cont.ToString();

        if (cont >= 5)
        {
            textoGanaste.text = "Ganaste!";
            StartCoroutine(ReiniciarDespuesDeEspera(5f)); // Reinicia despu�s de 5 segundos
        }
    }

    IEnumerator ReiniciarDespuesDeEspera(float tiempoEspera)
    {
        yield return new WaitForSeconds(tiempoEspera);

        // Reinicia el juego
        ReiniciarJuego();
    }

    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);

        rb.AddForce(vectorMovimiento * rapidez);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable"))
        {
            cont++;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Enemigo"))
        {
            PlayerDeath();
        }
    }

    private void PlayerDeath()
    {
        ReiniciarJuego();
    }

    private void ReiniciarJuego() => UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);

    // Restablecer el estado de salto cuando el jugador toca el piso
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            enElPiso = true;
            saltosRealizados = 0;
        }
    }

    IEnumerator ContadorTiempo()
    {
        while (tiempoRestante > 0f)
        {
            tiempoRestante -= Time.deltaTime;

            // Actualiza el texto del tiempo en la interfaz de usuario
            ActualizarTextoTiempo();

            yield return null;
        }

        // Puedes hacer algo cuando el tiempo llega a cero, como mostrar un Game Over
    }

    // Funci�n para actualizar el texto del tiempo en la interfaz de usuario
    void ActualizarTextoTiempo()
    {
        // Aseg�rate de tener asignado el objeto de texto en el Inspector
        if (textoTiempo != null)
        {
            // Muestra el tiempo restante en segundos con un formato espec�fico
            textoTiempo.text = "Tiempo: " + Mathf.Ceil(tiempoRestante).ToString();
        }
        else
        {
            Debug.LogError("El campo textoTiempo no est� asignado en el Inspector.");
        }
    }
}
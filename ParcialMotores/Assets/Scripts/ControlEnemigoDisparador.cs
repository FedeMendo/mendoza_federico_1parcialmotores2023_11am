using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ControlEnemigoDisparador : MonoBehaviour
{
    public GameObject bolitaPrefab;
    public Transform puntoDisparo;
    public float velocidadBolita = 15f;
    public float frecuenciaDisparo = 2f;

    void Start()
    {
        StartCoroutine(DispararBolitas());
    }

    IEnumerator DispararBolitas()
    {
        while (true)
        {
            yield return new WaitForSeconds(1 / frecuenciaDisparo);
            Disparar();
        }
    }

    void Disparar()
    {
        Vector3 direccionFija = new Vector3(1f, 0f, 0f); // Cambia esto seg�n la direcci�n deseada
        GameObject bolita = Instantiate(bolitaPrefab, puntoDisparo.position, Quaternion.identity);
        bolita.GetComponent<Rigidbody>().velocity = direccionFija * velocidadBolita;

        // Destruir la bolita despu�s de 3 segundos (ajusta el tiempo seg�n tus necesidades)
        Destroy(bolita, 5f);
    }
}
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ControlEnemigoDisparadorHaciaJugador : MonoBehaviour
{
    public GameObject bolitaPrefab;
    public Transform puntoDisparo;
    public float velocidadBolita = 5f; // Ajusta este valor seg�n tus necesidades
    public float frecuenciaDisparo = 2f;

    void Start()
    {
        StartCoroutine(DispararBolitas());
    }

    IEnumerator DispararBolitas()
    {
        while (true)
        {
            yield return new WaitForSeconds(1 / frecuenciaDisparo);
            Disparar();
        }
    }

    void Disparar()
    {
        GameObject jugador = GameObject.FindWithTag("Jugador"); // Aseg�rate de que el jugador tenga la etiqueta "Jugador"

        if (jugador != null)
        {
            Vector3 direccionAlJugador = (jugador.transform.position - puntoDisparo.position).normalized;
            GameObject bolita = Instantiate(bolitaPrefab, puntoDisparo.position, Quaternion.identity);
            bolita.GetComponent<Rigidbody>().velocity = direccionAlJugador * velocidadBolita;

            // Destruir la bolita despu�s de 3 segundos (ajusta el tiempo seg�n tus necesidades)
            Destroy(bolita, 6f);
        }
        else
        {
            Debug.LogWarning("No se encontr� el objeto con la etiqueta 'Jugador'.");
        }
    }
}
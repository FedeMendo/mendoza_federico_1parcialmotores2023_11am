using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRecolectable : MonoBehaviour
{
    public int aumentoRapidez = 1; // Aumento de velocidad del jugador al recolectar el �tem
    public float aumentoTama�o = 0.2f; // Aumento de tama�o del jugador al recolectar el �tem

    private bool recolectado = false; // Bandera para controlar si el �tem ha sido recolectado

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador") && !recolectado)
        {
            // Recolecta el �tem y aplica los aumentos al jugador
            ControlJugador jugador = other.GetComponent<ControlJugador>();
            if (jugador != null)
            {
                jugador.RecolectarItem(aumentoRapidez, aumentoTama�o);

                // Resta una llave al contador del jugador
                jugador.RestarLlave();
            }

            // Marca el �tem como recolectado
            recolectado = true;

            // Desactiva el objeto recolectable (si prefieres destruirlo, puedes usar Destroy(gameObject))
            gameObject.SetActive(false);
        }
    }
}
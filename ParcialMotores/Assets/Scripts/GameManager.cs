using System.Collections;
using UnityEngine;
using UnityEngine.UI; // Agregu� el espacio de nombres para el componente UI
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI textoGameOver; // Referencia al objeto de texto para mostrar Game Over

    void Update()
    {
        // Reinicia el juego al presionar la tecla "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
    }

    void RestartGame()
    {
        // Reinicia el nivel cargando la escena actual
        Scene currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        UnityEngine.SceneManagement.SceneManager.LoadScene(currentScene.name);
    }

    void MostrarGameOver()
    {
        // Muestra el letrero de Game Over
        textoGameOver.gameObject.SetActive(true);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigo : MonoBehaviour
{
    public float velocidad = 5f;
    public float limiteIzquierdo = -5f; // Definir el l�mite izquierdo del movimiento
    public float limiteDerecho = 5f;   // Definir el l�mite derecho del movimiento
    private bool moviendoseDerecha = true;

    void Update()
    {
        // Movimiento de un lado a otro
        if (moviendoseDerecha)
            transform.Translate(Vector3.right * velocidad * Time.deltaTime);
        else
            transform.Translate(Vector3.left * velocidad * Time.deltaTime);

        // Verificar l�mites y cambiar direcci�n
        if (transform.position.x >= limiteDerecho)
        {
            moviendoseDerecha = false;
        }
        else if (transform.position.x <= limiteIzquierdo)
        {
            moviendoseDerecha = true;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPared : MonoBehaviour
{
    public float velocidadSubida = 2f;
    public float velocidadBajada = 1f;
    public float tiempoEspera = 1f;

    private bool subiendo = true;

    void Update()
    {
        if (subiendo)
        {
            SubirPared();
        }
        else
        {
            BajarPared();
        }
    }

    void SubirPared()
    {
        transform.Translate(Vector3.up * velocidadSubida * Time.deltaTime);

        if (transform.position.y >= 5f) // Ajusta el l�mite superior seg�n la posici�n de tu pared
        {
            subiendo = false;
            Invoke("IniciarBajada", tiempoEspera);
        }
    }

    void BajarPared()
    {
        transform.Translate(Vector3.down * velocidadBajada * Time.deltaTime);

        if (transform.position.y <= 0f) // Ajusta el l�mite inferior seg�n la posici�n de tu pared
        {
            subiendo = true;
        }
    }

    void IniciarBajada()
    {
        subiendo = true; // Corregido el nombre del m�todo y cambiado el valor a true
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolitaControl : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            PlayerDeath();
        }
    }
    private void PlayerDeath()
    {
        // Aqu� puedes poner cualquier l�gica relacionada con la muerte del jugador
        // Por ejemplo, reiniciar el nivel, mostrar un mensaje de game over, etc.

        // En este ejemplo, simplemente desactivamos el GameObject del jugador
        ReiniciarJuego();
    }
    private void ReiniciarJuego()
    {
        // Reinicia el nivel cargando la escena actual
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}